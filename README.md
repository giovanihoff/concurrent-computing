# Programação Concorrente

Este projeto visa atender as atividades relacionadas à disciplina Programacão Concorrente, ministrada pelo professor Dr. Fernando Luís Dotti.

## Pré-requisitos:

1. Sistema de automação de compilação **Gradle**, versão 6.2.2 ou superior, devidamente instalado no sistema:
*  [ ] `$ brew update`
*  [ ] `$ brew install gradle`
2. JDK **JAVA**, versão 11, devidamente instalado no sistema:
*  [ ] `brew tap homebrew/cask-versions`
*  [ ] `brew cask install java`
3. Atualizar o arquivo de perfil "*.profile*" com as seguintes linhas:

> export JAVA_8_HOME=$(/usr/libexec/java_home -v1.8) 

> export JAVA_11_HOME=$(/usr/libexec/java_home -v11)

> alias java8='export JAVA_HOME=$JAVA_8_HOME'

> alias java11='export JAVA_HOME=$JAVA_11_HOME'

> java11

## Clone do Projeto:

*  `$ git clone git@gitlab.com:giovanihoff/concurrent-computing.git`
*  `$ cd concurrent-computing`

## Compilação:

*  `$ gradle clean build`

## Exercício 2:

*  Montar arcabouço para avaliação de desempenho para trabalhar com estas listas.
*  Ver a definição da interface e semântica de operações.
*  Para avaliação de desempenho sugere-se os seguintes parâmetros de configuração:
1.  Tamanho inicial da lista. Popular a lista inicialmente. Testar diferentes tamahos pois gerarão diferentes tempos de operação. Ex.: 100k, 200k, 300k
2.  Tamanho máximo da lista. Limitar o crescimento da lista até determonado valor. Ex.: 300k, 500k, 1000k.
3.  Distribuição de comandos. Que percentual teremos de adds, reads e removes. Sugere-se testar com mesma frequencia. Em outra situação aumentar os reads.
4.  Número de threads executoras. Variar o máximo permitido pela infraestrutura disponível. Não faz sentido passar do número de threads, considerando hyper-threading.
*  Tempo de execução e warm-up. Definir um tempo inicial onde medidas não são tomadas. A seguir iniciar medidas por um tempo de duração do experimento. Sugere-se 10 s de warmup e 1 min de experimento. Entretanto estes valores devem ser investigados empiricamente.
Métricas a serem observadas. As seguintes métricas são de interesse:
1.  Vazão geral do sistema em operações realizadas.
2.  Vazão por tipo de operação.
3.  Tamanho médio da lista.
*  Montar os testes e aplicar tanto aa lista coarse como fine grained para próxima aula.

### Resultados Obtidos:
*  Os resultados podem saer obtidos no diretório "**result**".

## Exercício 1:

*  Faça um programa com duas threads que acessam uma mesma variável, em loop;
*  Em uma das versões, utilize a implementação de lock pelo algoritmo de Peterson;
*  Em outra, utilize locks da api de Java;
*  Compare o desempenho. O método de comparação também faz parte da questão;
*  A entrega deve conter os fontes comentados;
*  Além da avaliação de desempenho (quanto uma solução é mais rápida que outra) e discussão da razão dos resultados.

### Descrição da Implementação:
*  O projeto está dividido em duas (2) classes principais:
1.  [src/main/**java**] br.com.cc.*CountLockJavaAPI* - Implementação de Lock pela API de Java;
2.  [src/main/**java**] br.com.cc.*CountLockPeterson* - Implementação de Lock pelo algoritmo de Peterson.
*  O algoritmo está baseado no **Contador de M. Ben-Ari**, conforme primeiro exemplo de aula (Aula 1);
*  Como critério para o sucesso do teste, é verificado se o valor de "**n**" é maior ou igual ao número de iterações;
*  A avaliação de desempenho é realizada através de teste unitário, para ambas as classes, utilizando **JUnit5** e através da seguinte classe:
1.  [src/main/**test**] br.com.cc.*CountTest* - Teste unitário que valida as classes Count.
*  Durante a execução, o teste é repetido oito (8) vezes, para cada classe, de forma a gerar diversos valores de comparação;
*  A análise do resultado desta avaliação de desempenho, pode ser verificada no relatório gerado pelo Gradle, onde consta a duração de CADA teste executado.

### Resultados Obtidos:
*  A implementação de Lock pela API de Java, se demonstrou mais performática que pelo algoritmo de Peterson;
*  O valor de "**n**" ainda se mostrou aleatório, mas maior que o número de iteracões;
*  Os resultados estão disponíveis no seguinte *link*:
1.  https://giovanihoff.gitlab.io/concurrent-computing/build/reports/tests/test/classes/br.com.cc.CountTest.html

### Análise dos Resultados:

*  `$ open build/reports/tests/test/classes/br.com.cc.CountTest.html`

## Observações:

*  A visualização dos resultados também pode ser obtida através do seguinte link: [concurrent-computing](https://giovanihoff.gitlab.io/concurrent-computing/)

## Licença:

Copyright © 2020, Giovani Hoff [giovanihoff@gmail.com].