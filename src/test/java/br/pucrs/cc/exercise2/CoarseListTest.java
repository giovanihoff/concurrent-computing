package br.pucrs.cc.exercise2;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

import br.pucrs.cc.data.ListData;
import br.pucrs.cc.exercise2.CoarseList;
import br.pucrs.cc.utils.LineChart;
import br.pucrs.cc.utils.TestRule;
import lombok.extern.java.Log;

@Log
@TestInstance(PER_CLASS)
class CoarseListTest extends TestRule {
	
	CoarseList<Integer> instance;
	int listSize = 0;
	int seriesSize = 0;
	int cont = 0;
	int flag = 0;
	
	@BeforeAll
	public void initAll() {
		setDataset( LineChart.createSeriesCollection() );
		instance = new CoarseList<Integer>();
	}
	
	@BeforeEach
	public void initEach() {

	}
	
	@ParameterizedTest
    @MethodSource("br.pucrs.cc.dataprovider.ListDataProvider#getData")
	@Order(1)
	@Timeout(60) // warm-up
	@DisplayName("Parallel Adds, Removes")
	public void parallelBothTest(ListData data) throws InterruptedException {
		log.info("Parallel both");
        
		if (listSize == 0 || listSize != data.getTestSize()) {
			seriesSize = listSize;
			listSize = data.getTestSize();
			setSeriesBackup( getSeries() );
			setSeries(LineChart.createSeries( Integer.toString(data.getTestSize()) ));
		}
		
		setThread(new Thread[data.getThreads()]);
		long timeout = System.currentTimeMillis();
		
        Thread[] myThreads = new Thread[2 * data.getThreads()];
        
        for (int i = 0; i < data.getThreads(); i++) {
          myThreads[i] = new AddThread(i * (listSize / data.getThreads()), listSize, data.getThreads());
          myThreads[i + data.getThreads()] = new RemoveThread(i * (listSize / data.getThreads()), listSize);
        }
        
        for (int i = 0; i < 2 * data.getThreads(); i ++) {
          myThreads[i].start();
        }
        
        for (int i = 0; i < 2 * data.getThreads(); i ++) {
          myThreads[i].join();
        }
        
        log.info("Size: " + listSize);
        int x = data.getThreads();
        log.info("X: " + x);
        long y = (System.currentTimeMillis()-timeout);
        log.info("Y: " + y);
        getSeries().add( x, y );
	}
	
	class AddThread extends Thread {
		int value;
		Integer testSize;
		Integer threads;
		
		AddThread(int i, int s, int t) {
			value = i;
			testSize = s;
			threads = t;
		}
		
		public void run() {
			for (int i = 0; i < (testSize / threads); i++) {
				if (value + i == testSize) {
					log.info("Test size reached!");
		        }
				instance.add(value + i);
			}
		}
	}
	
	class RemoveThread extends Thread {
		int value;
		Integer testSize;
		
		RemoveThread(int i, int s) {
			value = i;
			testSize = s;
		}
		
		public void run() {
			for (int i = 0; i < (testSize / testSize); i++) {
		        if (!instance.remove(value + i)) {
		        	failStatus("RemoveThread: duplicate remove: " + (value + i));
		        }
			}
		}
		
		public void failStatus(String text) {
			fail(text);
		}
	}
	
	@AfterEach
	public void endEach() {
		
		if (seriesSize == 0) {
			seriesSize = listSize;
		}
		
		if (listSize != seriesSize) {
			getDataset().addSeries(getSeriesBackup());
			seriesSize = listSize;
		}
	}
	
	@AfterAll
	public void endAll() {
		getDataset().addSeries(getSeries());
		LineChart.showChart(getDataset(), "CoarseList");
	}
	
}
