package br.pucrs.cc.utils;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

//@Data
public class TestRule {
	
	private Thread[] thread;
	private XYSeries series;
	private XYSeries seriesBackup;
	private XYSeriesCollection dataset;

	public XYSeries getSeries() {
		return series;
	}

	public void setSeries(XYSeries series) {
		this.series = series;
	}

	public Thread[] getThread() {
		return thread;
	}

	public void setThread(Thread[] thread) {
		this.thread = thread;
	}

	public XYSeriesCollection getDataset() {
		return dataset;
	}

	public void setDataset(XYSeriesCollection dataset) {
		this.dataset = dataset;
	}

	public XYSeries getSeriesBackup() {
		return seriesBackup;
	}

	public void setSeriesBackup(XYSeries seriesBackup) {
		this.seriesBackup = seriesBackup;
	}

}
