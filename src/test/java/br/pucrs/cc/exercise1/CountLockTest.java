package br.pucrs.cc.exercise1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.RepeatedTest;

/* 
 * A avaliação de desempenho é realizada através de teste unitário, para ambas as classes, utilizando JUnit5
 * Como critério para o sucesso do teste, é verificado se o valor de "n" é maior que o número de iterações
 * Durante a execução, o teste é repetido oito (8) vezes, para cada classe, de forma a gerar diversos valores de comparação
 * A análise do resultado desta avaliação de desempenho, pode ser verificada no relatório gerado pelo Gradle, onde consta a duração de CADA teste executado
 */

@Disabled
class CountLockTest {
	
	@Order(1)
	@RepeatedTest(value = 8, name = RepeatedTest.LONG_DISPLAY_NAME)
	void validateCountLockJavaAPI() {
    	
    	CountLockJavaAPI p = new CountLockJavaAPI();
    	CountLockJavaAPI q = new CountLockJavaAPI();
        
        p.start();
        q.start();
        
        try {
        	p.join();
        	q.join();
        } catch (InterruptedException e) { }
        
        System.out.println("[LockJavaAPI ] The value of 'n' is " + CountLockJavaAPI.getN() );

        assertTrue(CountLockJavaAPI.n >= CountLockJavaAPI.getIterations() );
    }
    
	@Order(2)
	@RepeatedTest(value = 8, name = RepeatedTest.LONG_DISPLAY_NAME)
	void validateCountLockPeterson() {
    	
    	CountLockPeterson p = new CountLockPeterson(0);
    	CountLockPeterson q = new CountLockPeterson(1);
        
        p.start();
        q.start();
        
        try {
        	p.join();
        	q.join();
        } catch (InterruptedException e) { }
        
        System.out.println("[LockPeterson] The value of 'n' is " + CountLockPeterson.getN() );

        assertTrue(CountLockJavaAPI.n >= CountLockJavaAPI.getIterations() );
    }
}
