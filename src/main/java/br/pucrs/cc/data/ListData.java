package br.pucrs.cc.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ListData {
	private Integer testSize;
	private Integer threads;
}