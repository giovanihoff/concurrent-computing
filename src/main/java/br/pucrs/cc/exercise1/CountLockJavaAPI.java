package br.pucrs.cc.exercise1;

import java.util.concurrent.locks.ReentrantLock;

/*
 * Implementação de Lock pela API de Java
 * O algoritmo está baseado no Contador de M. Ben-Ari, conforme primeiro exemplo de aula (Aula 1)
 */

class CountLockJavaAPI extends Thread {
	
	ReentrantLock l = new ReentrantLock();
	
    private static final int ITERATIONS = 10000000;
    
    static volatile int n = 0;

    @Override
    public void run() {
        for (int i = 0; i < getIterations() ; i++) {
        	l.lock();
        	setN(n + 1);
        	l.unlock();
        }
    }

    public static int getIterations() {
		return ITERATIONS;
	}

	public static int getN() {
		return n;
	}
	
	public static void setN( int value ) {
		n = value;
	}
	
}
