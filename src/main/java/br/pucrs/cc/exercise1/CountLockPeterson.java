package br.pucrs.cc.exercise1;

/*
 * Implementação de Lock pelo algoritmo de Peterson
 * O algoritmo está baseado no Contador de M. Ben-Ari, conforme primeiro exemplo de aula (Aula 1)
 */

class CountLockPeterson extends Thread {
	
    private static final int ITERATIONS = 10000000;
    
    private static boolean[] in = { false, false };
    
    private static int id = 0;
    private static volatile int n = 0;
    
    public CountLockPeterson(int i) {
        id = i;
    }
    
    private static int other() {
        return id == 0 ? 1 : 0;
    }

    @Override
    public void run() {
        for (int i = 0; i < getIterations() ; i++) {
        	lock();
        	setN(n + 1);
        	unlock();
        }
    }
    
    public static void lock() {
        in[id] = true;
        int turn = other();
        while( in[other()] && turn == other() );
    }

    public static void unlock() {
    	in[id] = false;
    }

    public static int getIterations() {
		return ITERATIONS;
	}

	public static int getN() {
		return n;
	}
	
	public static void setN( int value ) {
		n = value;
	}
	
}
