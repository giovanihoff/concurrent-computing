package br.pucrs.cc.dataprovider;

import br.pucrs.cc.data.ListData;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ListDataProvider {

    public static Object[][] getData() {
    	
        ListData data01 = ListData.builder()
        		.threads(8)
                .testSize(512)
                .build();
        ListData data02 = ListData.builder()
        		.threads(16)
                .testSize(512)
                .build();
        ListData data03 = ListData.builder()
        		.threads(24)
                .testSize(512)
                .build();
        
        ListData data11 = ListData.builder()
        		.threads(8)
                .testSize(1024)
                .build();
        ListData data12 = ListData.builder()
        		.threads(16)
                .testSize(1024)
                .build();
        ListData data13 = ListData.builder()
        		.threads(24)
                .testSize(1024)
                .build();
        
        ListData data21 = ListData.builder()
        		.threads(8)
                .testSize(2048)
                .build();
        ListData data22 = ListData.builder()
        		.threads(16)
                .testSize(2048)
                .build();
        ListData data23 = ListData.builder()
        		.threads(24)
                .testSize(2048)
                .build();
        
        return new Object[][]{
                {data01}, {data02}, {data03},
                {data11}, {data12}, {data13},
                {data21}, {data22}, {data23},
        };
    }
    
}
