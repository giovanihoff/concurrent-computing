package br.pucrs.cc.utils;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import lombok.extern.java.Log;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

@Log
public class LineChart extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private LineChart(XYDataset dataset, String title) {
    	initializeUI(dataset, title);
    }

	private void initializeUI(XYDataset dataset, String title) {
        JFreeChart chart = createChart(dataset, title);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
        chartPanel.setBackground(Color.white);
         
		add(chartPanel);

		pack();
		setTitle("Line Chart - " + title);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		try {
			ChartUtils.saveChartAsPNG(new File("result/" + title.replace( " ", "_" ) + ".png"), chart, 450, 400);
		} catch (IOException e) {
			log.log(Level.SEVERE, "Error to save chart as PNG!", e);
		}
    }

	private static JFreeChart createChart(final XYDataset dataset, String title) {
        JFreeChart chart = ChartFactory.createXYLineChart(
        		title,
                "Threads",
                "Throughput(ms)",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                true
        );

        XYPlot plot = chart.getXYPlot();

        var renderer = new XYLineAndShapeRenderer();

        renderer.setSeriesPaint(0, Color.RED);
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));
        renderer.setSeriesPaint(1, Color.BLUE);
        renderer.setSeriesStroke(1, new BasicStroke(2.0f));
        renderer.setSeriesPaint(2, Color.GREEN);
        renderer.setSeriesStroke(2, new BasicStroke(2.0f));

        plot.setRenderer(renderer);
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinesVisible(false);
        plot.setDomainGridlinesVisible(false);

        chart.getLegend().setFrame(BlockBorder.NONE);

        chart.setTitle(new TextTitle(title,
                        new Font("Serif", Font.BOLD, 18)
                )
        );
        return chart;
    }
	
	public static XYSeries createSeries(String name) {
		return new XYSeries(name);
	}
	
	public static XYSeriesCollection createSeriesCollection() {
		return new XYSeriesCollection();
	}
	
    public static void showChart(XYDataset dataset, String title) {
        var ex = new LineChart(dataset, title);
        ex.setVisible(true);
    }

}